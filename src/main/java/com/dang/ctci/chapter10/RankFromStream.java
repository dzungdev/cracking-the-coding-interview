package com.dang.ctci.chapter10;

class RankNode {

  public int leftSize = 0;
  public RankNode left, right;
  public int data = 0;
  public RankNode(int d) {
    data = d;
  }

  public void insert(int d) {
    if (d <= data) {
      if (left != null) {
        left.insert(d);
      } else {
        left = new RankNode(d);
        leftSize++;
      }
    } else {
      if (right != null) {
        right.insert(d);
      } else {
        right = new RankNode(d);
      }
    }
  }

  public int getRank(int d) {
    if (d == data) {
      return leftSize;
    } else if (d < data) {
      if (left == null) {
        return -1;
      } else {
        return left.getRank(d);
      }
    } else {
      int rightRank = right == null ? -1 : right.getRank(d);
      if (rightRank == -1) return -1;
      else return leftSize + 1 + rightRank;
    }
  }

}

public class RankFromStream {

  RankNode root = null;

  void track(int number) {
    if (root == null) {
      root = new RankNode(number);
    } else {
      root.insert(number);
    }
  }

  int getRankOfNumber(int number) {
    return root.getRank(number);
  }

  public static void main(String[] args) {
    RankFromStream rfs = new RankFromStream();
    rfs.track(5);
    rfs.track(13);
    rfs.track(10);
    rfs.track(15);
    rfs.track(23);
    rfs.track(24);
    rfs.track(25);
    rfs.track(20);

    System.out.println(rfs.getRankOfNumber(23));
  }

}


