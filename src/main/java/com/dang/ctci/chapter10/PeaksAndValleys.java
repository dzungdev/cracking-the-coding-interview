package com.dang.ctci.chapter10;

import java.util.Arrays;

public class PeaksAndValleys {

  int getBiggestIndex(int[] arr, int a, int b, int c) {
    int len = arr.length;

    int aValue = a >= 0 && a < len ? arr[a] : Integer.MIN_VALUE;
    int bValue = b >= 0 && b < len ? arr[b] : Integer.MIN_VALUE;
    int cValue = c >= 0 && c < len ? arr[c] : Integer.MIN_VALUE;

    int max = Math.max(aValue, Math.max(bValue, cValue));

    return aValue == max ? a : (bValue == max ? b : c);
  }

  private void swap(int[] arr, int source, int dest) {
    int temp = arr[source];
    arr[source] = arr[dest];
    arr[dest] = temp;
  }

  void sortValleyPeak(int[] arr) {
    for (int i = 1; i < arr.length; i += 2) {
      int biggestIndex = getBiggestIndex(arr,i -1, i, i+1);
      if (i != biggestIndex) {
        swap(arr, i, biggestIndex);
      }
    }
  }

  public static void main(String[] args) {
    int[] arr = new int[] {5,3,1,2,3};
    PeaksAndValleys app = new PeaksAndValleys();
    app.sortValleyPeak(arr);
    Arrays.stream(arr).forEach(e -> {
      System.out.print(e + " ");
    });
  }
}
