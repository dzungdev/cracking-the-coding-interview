package com.dang.ctci.chapter10;

public class SparseSearch {

  public int search(String[] strings, String keyword, int start, int end) {
    if (start > end) return -1;

    int mid = (start + end)/2;
    while (true) {
      int left = mid -1;
      int right = mid + 1;

      if (left < start || right > end) {
        return -1;
      } else if (left >= start && !strings[left].isEmpty()) {
        mid = left;
        break;
      } else if (right <= end && !strings[right].isEmpty()) {
        mid = right;
        break;
      }
      left --;
      right++;
    }

    if (strings[mid] == keyword) {
      return mid;
    } else if (strings[mid].compareTo(keyword) < 0) {
      return search(strings, keyword, mid + 1, end);
    } else {
      return search(strings, keyword, start, mid - 1);
    }
  }
}
