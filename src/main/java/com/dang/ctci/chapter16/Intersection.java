package com.dang.ctci.chapter16;

class Point {
  public double x,y;
  public Point(double x, double y) {
    this.x = x;
    this.y = y;
  }

  public void setLocation(double x, double y) {
    this.x = x;
    this.y = y;
  }
}

class Line {
  public double slope, yintercept;

  public Line(Point start, Point end) {
    double deltaY = end.y - start.y;
    double deltaX = end.x - start.x;
    slope = deltaY / deltaX;
    yintercept = end.y - slope * end.x;
  }
}

public class Intersection {

  private void swap(Point p1, Point p2) {
    double x = p1.x;
    double y = p1.y;
    p1.setLocation(p2.x, p2.y);
    p2.setLocation(x, y);
  }

  private boolean isBetween(double start, double end, double value) {
    if (start > end) {
      return end <= value && value <= start;
    } else {
      return start <= value && value <= end;
    }
  }

  private boolean isBetween(Point start, Point end, Point value) {
    return isBetween(start.x, end.x, value.x) &&
            isBetween(start.y, end.y, value.y);
  }

  public Point intersection(Point start1, Point end1, Point start2, Point end2) {
    if (start1.x > end1.x) swap(start1, end1);
    if (start2.x > end2.x) swap(start2, end2);
    if (start1.x > start2.x) {
      swap(start1, start2);
      swap(end1, end2);
    }

    Line line1 = new Line(start1, end1);
    Line line2 = new Line(start2, end2);

    if (line1.slope == line2.slope) {
      if (line1.yintercept == line2.yintercept && isBetween(start1, end1, start2)) {
        return start2;
      }
      return null;
    }

    //Get intersection coordinate
    double x = (line2.yintercept - line1.yintercept) / (line1.slope - line2.slope);
    double y = x * line1.slope + line2.yintercept;
    Point intersection = new Point(x,y);

    if (isBetween(start1, end1, intersection) &&
          isBetween(start2, end2, intersection)) {
      return intersection;
    }

    return null;
  }

}
