package com.dang.ctci.chapter16;


import java.util.Arrays;

class Person {
  public int birth;
  public int death;

  public Person(int birth, int death) {
    this.birth = birth;
    this.death = death;
  }

}

public class LivingPeople {

  int[] getSortedYears(Person[] people, boolean getBirth) {
    int[] years = new int[people.length];
    for (int i = 0; i < people.length; i++) {
      years[i] = getBirth ? people[i].birth : people[i].death;
    }
    Arrays.sort(years);
    return years;
  }

  public int getMaxAliveYear(Person[] people) {
    int[] birthYears = getSortedYears(people, true);
    int[] deathYears = getSortedYears(people, false);

    int birthIdx = 0;
    int deathIdx = 0;
    int maxAlivePeopleNum = 0;
    int curMaxAlivePeopleNum = 0;
    int maxAliveYear = 0;

    while (birthIdx < birthYears.length) {
      if (birthYears[birthIdx] <= deathYears[deathIdx]) {
        curMaxAlivePeopleNum++;
        if (curMaxAlivePeopleNum > maxAlivePeopleNum) {
          maxAlivePeopleNum = curMaxAlivePeopleNum;
          maxAliveYear = birthYears[birthIdx];
        }
        birthIdx++;
      } else {
        curMaxAlivePeopleNum--;
        deathIdx++;
      }
    }
    return maxAliveYear;
  }

}
